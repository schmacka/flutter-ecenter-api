import 'package:ecenter_api/ecenter_api.dart';

class Entity{
  ClientIDAttribute _id = ClientIDAttribute.nullId;
  Map<String, dynamic> _json = {};

  Entity({ClientIDAttribute? id , Map<String, dynamic>? json}) {
    if (json != null) {
      this._json = json;
      this._id = ClientIDAttribute.fromJson(json["_id"]);
    }
    if (id != null) {
      _id = id;
    }
  }

  Entity.fromJson(dynamic json) {
    _id = ClientIDAttribute.fromJson(json["_id"]);
    if (json is Map<String, dynamic>) {
      _json = json;
    }
  }

  ClientIDAttribute get id => _id;
  Map<String, dynamic>? get json => _json;
}
