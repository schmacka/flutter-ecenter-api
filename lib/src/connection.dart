import 'dart:math';

import 'package:ecenter_api/src/login_plugin.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'login_result.dart';

class Connection extends ChangeNotifier {
  String _adapterUrl;
  String _system;
  ILoginPlugin? _loginPlugin;
  String? _token;
  bool _loginInProgress = false;

  Connection(this._adapterUrl, this._system, {String? token}) {
    this._token = token;
  }

  Future<bool> loginInitialized() async{
    if(_loginPlugin == null) return false;
    var newToken = await _loginPlugin!.loginInitialized();
    if(newToken != null) {
      _token = newToken;
      notifyListeners();
      return true;
    }
    return false;
  }

  Future<LoginResult> login() async{
    if(_loginPlugin == null) {
      throw new Exception('Failed to login: No Login Plugin has been set.');
    }
    _loginInProgress = true;
    notifyListeners();
    try {
      LoginResult loginResult = await _loginPlugin!.login();
      if (loginResult.loginSuccessful != null && loginResult.loginSuccessful!) {
        _token = loginResult.token;
      }
      _loginInProgress = false;
      notifyListeners();
      return loginResult;
    }catch (e) {
      _loginInProgress = false;
      notifyListeners();
      throw (e);
    }
  }

  bool get loginInProgress => _loginInProgress;

  set loginPlugin(ILoginPlugin value) {
    _loginPlugin = value;
  }

  String get adapterUrl => _adapterUrl;

  String get system => _system;

  String? getToken() {
    return _token;
  }

  bool isLoggedIn() {
    return _token != null;
  }

  bool isLoginInProgress() {
    return this._loginInProgress;
  }

  bool invalidateToken(){
    _token = null;
    return true;
  }
}