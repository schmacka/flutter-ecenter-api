class ClientIDAttribute {
  static const nullSystem = "null";
  static const nullType = "null";
  static const nullVersion = "null";
  static const _nullValue = nullSystem + "*" + nullType + "*" + nullVersion + "***";
  static var idMatcher = RegExp(r"([^*]*)\*([^*]*)\*([^*]*)\*\*\*(.*)", caseSensitive: false, multiLine: false);
  static final ClientIDAttribute nullId =  ClientIDAttribute(system: nullSystem, type: nullType, version: nullVersion, id: "");

  String _value = _nullValue;

  String get value => _value;

  ClientIDAttribute({String? system, String? type, String? version, String? id}) {
    _value = system.toString() + "*" + type.toString() + "*" + version.toString() + "***" + (id == null ? "" : id);
  }

  ClientIDAttribute.fromJson(dynamic json) {
    if (json == null) {
      _value = _nullValue;
      return;
    }
    if (json is String) {
      _value = json;
    } else {
      if (json["value"] == null) {
        throw Exception("Unable to parse json id without value element:'$json'");
      }
      _value = json["value"] ?? _nullValue;
    }
    if (!idMatcher.hasMatch(_value)) {
      throw Exception("Id $_value does not match SYSTEM*TYPE*VERSION***ID");
    }
  }

  String get type {
    var split = _value.split("*");
    return split.length > 1 ? split[1] : ClientIDAttribute.nullType;
  }

  get system {
    var split = _value.split("*");
    return split.length > 1 ? split[0] : ClientIDAttribute.nullSystem;
  }

  get id {
    var split = _value.split("***");
    return split.length > 1 ? split[1] : "";
  }

  get version {
    var split = _value.split("*");
    return split.length > 2 ? split[2] : ClientIDAttribute.nullVersion;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["value"] = _value;
    return map;
  }
}
