class ErrorResult {
  String? _message;
  String? _causeToStringMessage;
  String? _causeType;
  int? _httpStatus;
  List<String>? _stackTrace;
  String? _serverInvocationID;
  List<ErrorResult>? _suppressed;

  String? get message => _message;

  String? get causeToStringMessage => _causeToStringMessage;

  String? get causeType => _causeType;

  int? get httpStatus => _httpStatus;

  List<String>? get stackTrace => _stackTrace;

  String? get serverInvocationID => _serverInvocationID;

  List<ErrorResult>? get suppressed => _suppressed;

  ErrorResult(
      {String? message,
      String? causeToStringMessage,
      String? causeType,
      int? httpStatus,
      List<String>? stackTrace,
      String? serverInvocationID,
      List<ErrorResult>? suppressed}) {
    _message = message;
    _causeToStringMessage = causeToStringMessage;
    _causeType = causeType;
    _httpStatus = httpStatus;
    _stackTrace = stackTrace;
    _serverInvocationID = serverInvocationID;
    _suppressed = suppressed;
  }

  ErrorResult.fromJson(dynamic json) {
    _message = json["message"];
    _causeToStringMessage = json["causeToStringMessage"];
    _causeType = json["causeType"];
    _httpStatus = json["httpStatus"];
    _stackTrace = json["stackTrace"] != null ? json["stackTrace"].cast<String>() : [];
    _serverInvocationID = json["serverInvocationID"];
    if (json["suppressed"] != null) {
      _suppressed = [];
      json["suppressed"].forEach((v) {
        _suppressed?.add(ErrorResult.fromJson(v));
      });
    }
  }
}
