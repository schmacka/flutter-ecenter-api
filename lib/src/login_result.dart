class LoginResult {
  bool? _loginSuccessful;
  String? _userName;
  List<String>? _allRoles;
  String? _token;

  bool? get loginSuccessful => _loginSuccessful;

  String? get userName => _userName;

  List<String>? get allRoles => _allRoles;

  String? get token => _token;

  LoginResult({bool? loginSuccessful, String? userName, List<String>? allRoles, String? token}) {
    _loginSuccessful = loginSuccessful;
    _userName = userName;
    _allRoles = allRoles;
    _token = token;
  }

  LoginResult.fromJson(dynamic json) {
    _loginSuccessful = json["loginSuccessful"];
    _userName = json["userName"];
    _allRoles = json["allRoles"] != null ? json["allRoles"].cast<String>() : [];
    _token = json["token"];
  }
}
