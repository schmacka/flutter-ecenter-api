
import 'package:ecenter_api/ecenter_api.dart';

typedef Entity jsonParser(Map<String, dynamic> json);

class EntityTypeRegistry {
  Map<String, jsonParser> registered = {};


  EntityTypeRegistry() {
    register(Entity, (json) => Entity.fromJson(json));
  }

  Entity fromJson(Map<String, dynamic> json, EntityTypeRegistry parser) {
    Entity e = Entity.fromJson(json);
    return getParserByName(e.id.type)(json);
  }

  register(Type clazz, jsonParser builder) {
    registered[clazz.toString()] = builder;
  }

  jsonParser getParser(Type t) {
    return getParserByName(t.toString());
  }

  jsonParser getParserByName(String name) {
    if (registered.containsKey(name)) {
      return registered[name]!;
    }

    var t = Entity;
    return registered[t.toString()]!;
  }


}