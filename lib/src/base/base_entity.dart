import 'package:ecenter_api/ecenter_api.dart';


class BaseEntity extends Entity{
  Entity? _creator;
  DateTime? _creationDate;
  DateTime? _lastModified;
  int? _versionStamp;
  String? _uuid;

  Entity? get creator => _creator;

  DateTime? get creationDate => _creationDate;

  DateTime? get lastModified => _lastModified;

  int? get versionStamp => _versionStamp;

  String? get uuid => _uuid;

  BaseEntity({id, creator, creationDate, lastModified, versionStamp, uuid, json})
      :super(id: id, json: json) {
    _creator = creator;
    _creationDate = creationDate;
    _lastModified = lastModified;
    _uuid = uuid;
    _versionStamp = versionStamp;
  }

  BaseEntity.fromJson(dynamic json)
      :super(json:json) {
    _creator = _parseCreator(json);
    _creationDate = json["CreationDate"] == null ? null : DateTime.parse(json["CreationDate"].toString());
    _lastModified = json['LastModified'] == null ? null : DateTime.parse(json['LastModified'].toString());
    _versionStamp = json["VersionStamp"];
    _uuid = json["UUID"];
  }

  Entity? _parseCreator(dynamic jsonInput){
    if(jsonInput["CreatorValue"] == null) return null;
    if(jsonInput["CreatorValue"]["value"] == null) return null;
    return Entity(id: ClientIDAttribute.fromJson(jsonInput["CreatorValue"]));
  }
}
