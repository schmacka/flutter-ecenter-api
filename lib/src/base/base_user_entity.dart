import 'package:ecenter_api/ecenter_api.dart';

class BaseUserEntity extends BaseEntity{
  bool? _adminUser;
  String? _creatorDisplay;
  String? _creatorName;
  bool? _enabled;
  String? _userName;

  bool? get adminUser => _adminUser;

  String? get creatorDisplay => _creatorDisplay;

  String? get creatorName => _creatorName;

  bool? get enabled => _enabled;

  String? get userName => _userName;

  BaseUserEntity({id, adminUser, creator, creationDate, lastModified, creatorDisplay, creatorName, enabled, userName, uuid, versionStamp, json})
      :super(id: id, creator: creator, creationDate: creationDate, lastModified: lastModified, versionStamp: versionStamp, uuid: uuid, json: json) {
    _adminUser = adminUser;
    _creatorDisplay = creatorDisplay;
    _creatorName = creatorName;
    _enabled = enabled;
    _userName = userName;
  }

  BaseUserEntity.fromJson(dynamic json) : super.fromJson(json) {
    _adminUser = json["AdminUser"];
    _creatorDisplay = json["CreatorDisplay"];
    _creatorName = json["CreatorName"];
    _enabled = json["Enabled"];
    _userName = json["UserName"];
  }
}
