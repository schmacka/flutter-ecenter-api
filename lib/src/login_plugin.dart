import '../ecenter_api.dart';

abstract class ILoginPlugin {
  Future<LoginResult> login();
  Future<bool> logout();
  Future<String?> loginInitialized();
}
