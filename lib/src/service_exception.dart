import 'error_result.dart';

class ServiceException implements Exception {
  String message;
  ErrorResult? error;
  int? httpStatusCode;

  ServiceException(this.message, [this.httpStatusCode, this.error]);

  String toString() => 'ServiceException: $message (http status code: $httpStatusCode)';
}
