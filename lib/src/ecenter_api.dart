library ecenter_api;

import 'dart:convert';

import 'package:ecenter_api/ecenter_api.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

/// eCenter
class ServiceApi {
  String url;
  late http.Client client;
  String? token;

  ServiceApi(this.url, {String? token, Client? httpClient}) {
    if (httpClient != null) {
      client = httpClient;
    } else {
      client = Client();
    }
    this.token = token;
  }

  Future<LoginResult> createToken(String username, String password) async {
    if (username.isEmpty) {
      throw "Username required.";
    }
    if (password.isEmpty) {
      throw "Password required.";
    }
    String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));

    var loginUrl = url + "/v2/sessions";

    Map<String, String> headers = {
      'content-type': 'application/json',
      'accept': 'application/json',
      'authorization': basicAuth
    };
    try {
      Response r = await client.post(Uri.parse(loginUrl), headers: headers);
      var json = handleError(r, loginUrl);
      var result = LoginResult.fromJson(json);
      token = result.token;
      return result;
    } catch (e) {
      print("Failed to POST to " + loginUrl + " : " + e.toString());
      throw e;
    }
  }

  Future<dynamic> callService<T>(String serviceName, List parameter, {List<String> expandattributes = const <String>[]}) async {
    var serviceUrl = url + "/v2/services/" + serviceName;

    Map<String, String> headers = {
      'content-type': 'application/json',
      'accept': 'application/json',
      'authorization': "Bearer " + token.toString(),
    };
    var request = {"parameter": parameter};
    if (expandattributes.length > 0) {
      request["expand"] = expandattributes;
    }
    try {
      Response r = await client.post(Uri.parse(serviceUrl),
          headers: headers, body: JsonEncoder().convert(request));
      var json = handleError(r, serviceUrl);
      return json;//call.fromJson(json) as T;
    } catch (e) {
      print("Failed to POST to " + serviceUrl + " : " + e.toString());
      throw e;
    }
  }



  dynamic handleError<T>(http.Response r, String serviceUrl) {
    if (r.statusCode == 200) {
      var json = jsonDecode(r.body);
      if (json is Map) {
        return json["result"];
      } else {
        throw Exception("Call to $serviceUrl failed, did not return proper result:" + r.body);
      }
      return json;
    }
    if (r.body.isNotEmpty) {
      var json;
      try {
        json = jsonDecode(r.body);
      } catch (e) {
        throw ServiceException("Call to $serviceUrl failed, server did not return further details.", r.statusCode);
      }

      ErrorResult result = ErrorResult.fromJson(json['result']);
      throw ServiceException(
          "Service failed, server message is '" + result.message.toString() + "'.", r.statusCode, result);
    }
    throw ServiceException("Service call to $serviceUrl failed.", r.statusCode);
  }
}
