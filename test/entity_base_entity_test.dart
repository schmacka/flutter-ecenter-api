import 'dart:convert';

import 'package:ecenter_api/src/base/base_entity.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  test('BaseEntity_valid_from_json', () {
    final json = jsonDecode(queryResult);
    final baseEntityJson = json["result"][0];
    final BaseEntity entity = BaseEntity.fromJson(baseEntityJson);

    expect(entity, isNotNull);
    expect(entity.versionStamp, 0);
    expect(entity.uuid, "62cefd49-ed38-4de2-a968-ce8ae93e5c0c");
    expect(entity.lastModified, DateTime.parse("2016-09-09T07:07:23.056Z"));
    expect(entity.creationDate, DateTime.parse("2016-09-09T07:07:23.056Z"));
    expect(entity.creator, isNotNull);
    expect(entity.creator!.id.value, "BASE*BaseUser*18***661c8dbe-7bc8-4afa-9949-8873f0cdc649");
  });

  test('BaseEntity_null_values_from_json', () {
    final json = jsonDecode(queryResult);
    final baseEntityJson = json["result"][1];
    final BaseEntity entity = BaseEntity.fromJson(baseEntityJson);

    expect(entity, isNotNull);
    expect(entity.versionStamp, isNull);
    expect(entity.uuid, isNull);
    expect(entity.lastModified, isNull);
    expect(entity.creationDate, isNull);
    expect(entity.creator, isNull);
    // expect(entity.creator!.id.value, "BASE*BaseUser*18***661c8dbe-7bc8-4afa-9949-8873f0cdc649");
  });

  test('BaseEntity_creator_null_from_json', () {
    final json = jsonDecode(queryResult);
    final baseEntityJson = json["result"][2];
    final BaseEntity entity = BaseEntity.fromJson(baseEntityJson);

    expect(entity, isNotNull);
    expect(entity.creator, isNull);
    // expect(entity.creator!.id.value, "BASE*BaseUser*18***661c8dbe-7bc8-4afa-9949-8873f0cdc649");
  });

  test('BaseEntity_values_missing_from_json', () {
    final json = jsonDecode(queryResult);
    final baseEntityJson = json["result"][3];
    final BaseEntity entity = BaseEntity.fromJson(baseEntityJson);

    expect(entity, isNotNull);
    expect(entity.versionStamp, isNull);
    expect(entity.uuid, isNull);
    expect(entity.lastModified, isNull);
    expect(entity.creationDate, isNull);
    expect(entity.creator, isNull);
    // expect(entity.creator!.id.value, "BASE*BaseUser*18***661c8dbe-7bc8-4afa-9949-8873f0cdc649");
  });
}

String queryResult = '''
  {
  "result": [
		{
			"_id":{
				"value":"BASE*ProcessStructureProcess*0***62cefd49-ed38-4de2-a968-ce8ae93e5c0c"
			},
			"CreationDate":"2016-09-09T07:07:23.056Z",
			"CreatorDisplay":"admin",
			"CreatorName":"admin",
			"CreatorValue":{
				"value":"BASE*BaseUser*18***661c8dbe-7bc8-4afa-9949-8873f0cdc649"
			},
			"CreatorValue_DISPLAY":"admin",
			"DisplayedName":"Dokument Process",
			"HelpText":null,
			"Id":808,
			"LastModified":"2016-09-09T07:07:23.056Z",
			"LocalizationMap":{},
			"Name":"Process1",
			"ProcessValidatorClassName":null,
			"QueueName":null,
			"TechnicalDescription":null,
			"TranslatedName":"Dokument Process",
			"UUID":"62cefd49-ed38-4de2-a968-ce8ae93e5c0c",
			"VersionStamp":0,
			"WorkflowName":null,
			"WorkflowTag":null
		},
		{
			"_id":{
				"value":"BASE*ProcessStructureProcess*0***f38f8316-1400-4af4-9b23-19bd68a8151d"
			},
			"CreationDate":null,
			"CreatorDisplay":"admin",
			"CreatorName":"admin",
			"CreatorValue":{
				"value":null
			},
			"CreatorValue_DISPLAY":"admin",
			"DisplayedName":"Anlage Prozess",
			"HelpText":null,
			"Id":1919,
			"LastModified":null,
			"LocalizationMap":{},
			"Name":"Process2",
			"ProcessValidatorClassName":null,
			"QueueName":null,
			"TechnicalDescription":null,
			"TranslatedName":"Anlage Prozess",
			"UUID":null,
			"VersionStamp":null,
			"WorkflowName":null,
			"WorkflowTag":null
		},
		{
			"_id":{
				"value":"BASE*ProcessStructureProcess*0***f38f8316-1400-4af4-9b23-19bd68a8151d"
			},
			"CreationDate":null,
			"CreatorDisplay":"admin",
			"CreatorName":"admin",
			"CreatorValue":null,
			"CreatorValue_DISPLAY":"admin",
			"DisplayedName":"Anlage Prozess",
			"HelpText":null,
			"Id":1919,
			"LastModified":null,
			"LocalizationMap":{},
			"Name":"Process2",
			"ProcessValidatorClassName":null,
			"QueueName":null,
			"TechnicalDescription":null,
			"TranslatedName":"Anlage Prozess",
			"UUID":null,
			"VersionStamp":null,
			"WorkflowName":null,
			"WorkflowTag":null
		},
		{
			"_id":{
				"value":"BASE*ProcessStructureProcess*0***f38f8316-1400-4af4-9b23-19bd68a8151d"
			}
		}
  ],
  "details": {
      "serverInvocationID": "06505ea2-5"
  }
}
  ''';
