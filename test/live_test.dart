import 'dart:io';

import 'package:ecenter_api/ecenter_api.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';

import 'testdata/base_user.dart';

void main() {


  test('login_to_tim_test_and_query', () async {
     final api = ServiceApi("https://tim-service-test.ecs-gmbh.de");
     LoginResult r  = await api.createToken("admin", "admin");
     Future f = api.callService("query", ["BaseUser", {}]);
     List result  = await f;
     for (var o in result) {
      var user = BaseUser.fromJson(o);
      print(user.userName);
    }
  });
}