import 'dart:io';

import 'package:ecenter_api/ecenter_api.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';

import 'testdata/base_user.dart';

void main() {
  MockClient mockClient() {
    return MockClient((request) async {
      return Response("", 400);
    });
  }

  test('URL is stored', () {
    final service = ServiceApi("http://test", httpClient: mockClient());
    expect(service.url, "http://test");
  });

  test('buildToken_withoutUsernameFails', () {
    final service = ServiceApi("http://test", httpClient: mockClient());
    expect(() => service.createToken("", "pass"),
        throwsA((description) => description == "Username required."));
  });

  test('buildToken_withoutPasswordFails', () {
    final service = ServiceApi("http://test", httpClient: mockClient());
    expect(() => service.createToken("user", ""),
        throwsA((description) => description == "Password required."));
  });

  test('successfulResponse_containsToken', () async {
    final service = ServiceApi("http://test", httpClient: MockClient((request) async {
      return Response(loginSuccessfultJson, 200);
    }));
    LoginResult r = await service.createToken("user", "pass");
    expect(r.token, isNotNull);
    expect(r.userName, equals("admin"));
    expect(service.token, isNotNull);
  });

  test('wrongLogin_causesFailture', () async {
    final service = ServiceApi("http://test", httpClient: MockClient((request) async {
      return Response(loginSuccessfultJson, 401);
    }));

    expect(() async {
      var r = await service.createToken("user", "pass");
    }, throwsA((Exception ex) => ex.toString().contains("401")));
  });

  test('exception_containsStatus', () async {
    final service = ServiceApi("http://test", httpClient: MockClient((request) async {
      return Response(loginSuccessfultJson, 401);
    }));

    expect(() async {
      await service.createToken("user", "pass");
    }, throwsA((ServiceException ex) => ex.httpStatusCode == 401));
  });

  test('unknownError_throwError', () async {
    final service = ServiceApi("http://test", httpClient: MockClient((request) async {
      throw SocketException("test");
    }));

    expect(() async {
      await service.createToken("user", "pass");
    }, throwsA((Exception ex) => ex.toString() == "SocketException: test"));
  });

  test('loginInvalidError_throwsExceptionContainingServerMessage', () async {
    final service = ServiceApi("http://test",httpClient:  MockClient((request) async {
      return new Response(loginInvalid, 401);
    }));

    expect(
      () async {
        await service.createToken("not", "present");
      },
      throwsA((ServiceException ex) =>
          ex.error?.message == "INVALID_LOGIN" && ex.error?.httpStatus == 401),
    );
  });

  test('testtest', () async {
     final service = ServiceApi("https://tim-service-test.ecs-gmbh.de");
     LoginResult r  = await service.createToken("admin", "admin");
     var api = ServiceApi("https://tim-service-test.ecs-gmbh.de", token: r.token);
     Future f = api.callService("query", ["BaseUser", {}]);
     List result  = await f;
     for (var o in result) {
      var user = BaseUser.fromJson(o);
      print(user.userName);
    }


  });
}

String loginSuccessfultJson = '''
{
    "result": {
        "loginSuccessful": true,
        "userName": "admin",
        "allRoles": [
            "tim_admin",
            "mam-admin",
            "tim_weekly_time_sheet_control",
            "tim_resourceplan",
            "tim"
        ],
        "sessionId": null,
        "token": "eyJhbGciOiJIUzUxMiIsImtpZCI6IkFVVEgifQ.eyJqdGkiOiJPZC1lSmVpczVvYyIsInN1YiI6ImFkbWluIiwiaWF0IjoxNjIyMTk0NzI4LCJleHAiOjE2MjIyMzA3MjgsImF1ZCI6InRpbW1hbSIsImlzcyI6IkJBU0UiLCJzY29wZSI6InRpbV9hZG1pbiBtYW0tYWRtaW4gdGltX3dlZWtseV90aW1lX3NoZWV0X2NvbnRyb2wgdGltX3Jlc291cmNlcGxhbiB0aW0ifQ.1Dj_9-Wvh1H7Qnp7OnG0dsXIonX5rDiC30SSK00jnqitRiLRfB8toxds6IFJJnKLIDJt-mdEmv2qZZqwtV0Ztg"
    },
    "details": {
        "clientSessionData": {
            "role-tim_admin": "true",
            "role-mam-admin": "true",
            "role-tim_resourceplan": "true",
            "role-tim_weekly_time_sheet_control": "true",
            "role-tim": "true",
            "BASE-push-timeout": "60000",
            "ResourcePlanInfoTable": "true"
        },
        "serverInvocationID": "f8736e86-3"
    }
}
''';

String loginInvalid = '''
{
    "result": {
        "message": "INVALID_LOGIN",
        "causeToStringMessage": null,
        "causeType": "com.iris.client.webservice.AdapterException",
        "status": null,
        "httpStatus": 401,
        "identifier": null,
        "attributeKey": null,
        "stackTrace": [
            "com.iris.base.services.authentication.DefaultLoginServicePlugin authenticateUser(DefaultLoginServicePlugin.java:56)",
            "com.iris.base.services.authentication.Login login(Login.java:71)",
            "jdk.internal.reflect.NativeMethodAccessorImpl invoke0(NativeMethodAccessorImpl.java:-2)",
            "jdk.internal.reflect.NativeMethodAccessorImpl invoke(NativeMethodAccessorImpl.java:62)",
            "jdk.internal.reflect.DelegatingMethodAccessorImpl invoke(DelegatingMethodAccessorImpl.java:43)",
            "java.lang.reflect.Method invoke(Method.java:566)"
        ],
        "serverInvocationID": "f8736e86-4",
        "suppressed": [
            {
                "message": null,
                "causeToStringMessage": "java.lang.reflect.InvocationTargetException",
                "causeType": "java.lang.reflect.InvocationTargetException",
                "status": null,
                "httpStatus": 0,
                "identifier": null,
                "attributeKey": null,
                "stackTrace": [
                    "jdk.internal.reflect.NativeMethodAccessorImpl invoke0(NativeMethodAccessorImpl.java:-2)",
                    "jdk.internal.reflect.NativeMethodAccessorImpl invoke(NativeMethodAccessorImpl.java:62)",
                    "jdk.internal.reflect.DelegatingMethodAccessorImpl invoke(DelegatingMethodAccessorImpl.java:43)",
                    "java.lang.reflect.Method invoke(Method.java:566)",
                    "com.iris.base.internal.ProxyInvocationHandler invoke(ProxyInvocationHandler.java:238)",
                    "java.lang.Thread run(Thread.java:834)"
                ],
                "serverInvocationID": "f8736e86-4",
                "suppressed": []
            }
        ]
    },
    "details": {
        "serverInvocationID": "f8736e86-4"
    }
}
''';
