import 'package:ecenter_api/ecenter_api.dart';


class BaseUser extends Entity{
  bool? _adminUser;
  String? _creationDate;
  String? _creatorDisplay;
  String? _creatorName;
  bool? _enabled;
  String? _userName;
  String? _uuid;
  int? _versionStamp;

  bool? get adminUser => _adminUser;

  String? get creationDate => _creationDate;

  String? get creatorDisplay => _creatorDisplay;

  String? get creatorName => _creatorName;

  bool? get enabled => _enabled;

  String? get userName => _userName;

  String? get uuid => _uuid;

  int? get versionStamp => _versionStamp;

  BaseUser({id, adminUser, creationDate, creatorDisplay, creatorName, enabled, userName, uuid, versionStamp, json}) :super(id: id, json: json){
    _adminUser = adminUser;
    _creationDate = creationDate;
    _creatorDisplay = creatorDisplay;
    _creatorName = creatorName;
    _enabled = enabled;
    _userName = userName;
    _uuid = uuid;
    _versionStamp = versionStamp;
  }

  BaseUser.fromJson(dynamic json): super(json:json) {
    _adminUser = json["AdminUser"];
    _creationDate = json["CreationDate"];
    _creatorDisplay = json["CreatorDisplay"];
    _creatorName = json["CreatorName"];
    _enabled = json["Enabled"];
    _userName = json["UserName"];
    _uuid = json["UUID"];
    _versionStamp = json["VersionStamp"];
  }
}
