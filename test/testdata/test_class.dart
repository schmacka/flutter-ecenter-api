import 'package:ecenter_api/ecenter_api.dart';


class TestClass extends Entity{
  TestClass({id, json}) :super(json:json){
  }

  TestClass.fromJson(dynamic json): super(json:json) {
  }
}

class TestClassChild extends TestClass{
  TestClassChild({id, json}) :super(id:id, json:json){
  }

  TestClassChild.fromJson(dynamic json): super(json:json) {
  }
}
