import 'package:ecenter_api/ecenter_api.dart';
import 'package:ecenter_api/src/login_plugin.dart';

class LoginPluginMock extends ILoginPlugin{

  Future<LoginResult> login() async{
    return LoginResult(loginSuccessful: true);
  }

  Future<bool> logout() async{
    return true;
  }

  @override
  Future<String?> loginInitialized() async{
    return null;
  }
}
