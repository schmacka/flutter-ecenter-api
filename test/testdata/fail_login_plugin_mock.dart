import 'package:ecenter_api/ecenter_api.dart';
import 'package:ecenter_api/src/login_plugin.dart';

class ThrowningLoginPluginMock extends ILoginPlugin{

  Future<LoginResult> login() async{
    throw "not expected here";
  }

  Future<bool> logout() async{
    return true;
  }

  @override
  Future<String?> loginInitialized() async{
    return null;
  }
}
