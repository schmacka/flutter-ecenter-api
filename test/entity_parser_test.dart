import 'dart:io';

import 'package:ecenter_api/ecenter_api.dart';
import 'package:flutter_test/flutter_test.dart';

import 'testdata/test_class.dart';


void main() {
  test("registeredParser_isReturned", () {
    EntityTypeRegistry p = EntityTypeRegistry();
    var function = (_) => Entity();
    p.register(TestClass, function);
    expect(p.getParser(TestClass), equals(function));

  });

  test("forUnknownType_entityParserIsReturned", () {
    EntityTypeRegistry p = EntityTypeRegistry();
    var function = (_) => Entity();
    p.register(Entity, function);
    expect(p.getParser(TestClass), equals(function));
  });


}