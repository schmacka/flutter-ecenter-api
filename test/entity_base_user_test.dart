import 'dart:convert';

import 'package:ecenter_api/ecenter_api.dart';
import 'package:ecenter_api/src/base/base_user_entity.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  test('BaseUser_valid_from_json', () {
    final json = jsonDecode(queryResult);
    final baseUserJson = json["result"][0];
    final BaseUserEntity baseUser = BaseUserEntity.fromJson(baseUserJson);
    expect(baseUser, isNotNull);
    expect(baseUser.userName, "admin");
    expect(baseUser.uuid, "661c8dbe-7bc8-4afa-9949-8873f0cdc649");
    expect(baseUser.versionStamp, 1);
    expect(baseUser.enabled, true);
    expect(baseUser.creatorName, "");
    expect(baseUser.creatorDisplay, "");
    expect(baseUser.creationDate, DateTime.parse("2007-11-05T23:00:00Z"));
    expect(baseUser.adminUser, true);
    expect(baseUser.id.value, "BASE*BaseUser*1***661c8dbe-7bc8-4afa-9949-8873f0cdc649");
  });

  test('BaseUser_null_values_from_json', () {
    final json = jsonDecode(queryResult);
    final baseUserJson = json["result"][1];
    final BaseUserEntity baseUser = BaseUserEntity.fromJson(baseUserJson);
    expect(baseUser, isNotNull);
    expect(baseUser.userName, null);
    expect(baseUser.uuid, "af70d55a-0a11-4783-bdd4-faae17227c52");
    expect(baseUser.versionStamp, 0);
    expect(baseUser.enabled, false);
    expect(baseUser.creatorName, isNull);
    expect(baseUser.creatorDisplay, isNull);
    expect(baseUser.creationDate, isNull);
    expect(baseUser.adminUser, false);
    var clientIDAttribute = ClientIDAttribute(system: "BASE", type: "BaseUser", version: "0", id: "af70d55a-0a11-4783-bdd4-faae17227c52");
    expect(baseUser.id.system, clientIDAttribute.system);
    expect(baseUser.id.type, clientIDAttribute.type);
    expect(baseUser.id.version, clientIDAttribute.version);
    expect(baseUser.id.id, clientIDAttribute.id);
  });
}

String queryResult = '''
  {
    "result": [
        {
            "_id": {
                "value": "BASE*BaseUser*1***661c8dbe-7bc8-4afa-9949-8873f0cdc649"
            },
            "AdminUser": true,
            "AnonymousLogin": false,
            "CreationDate": "2007-11-05T23:00:00Z",
            "CreatorDisplay": "",
            "CreatorName": "",
            "CreatorValue": null,
            "DailyHours": 8.0,
            "Display": "admin",
            "Display_DISPLAY": "admin",
            "DisplayedName": "admin",
            "Division": null,
            "DomainAccount": null,
            "Email": null,
            "Enabled": true,
            "Extern": false,
            "FirstName": null,
            "FullName": "admin",
            "HoursEachDay": 8.0,
            "HoursFriday": 0.0,
            "HoursMonday": 0.0,
            "HoursThursday": 0.0,
            "HoursTuesday": 0.0,
            "HoursWednesday": 0.0,
            "Id": 1,
            "Internal": false,
            "InvalidLoginAttempts": 0,
            "LastActivity": "2021-05-28T15:35:02.225+02:00",
            "LastModified": "2021-05-28T09:37:31.803Z",
            "LastName": null,
            "LastNameOrUserName": "admin",
            "LastPasswordChange": "2021-05-28T11:37:31.734+02:00",
            "Mobile": null,
            "Phone": null,
            "ResetMonthlyHours": false,
            "ResourcePlanAccess": false,
            "SerialNumber": null,
            "TimeSheetAccess": false,
            "TimesheetRequired": false,
            "Title": null,
            "UserName": "admin",
            "UserShortName": null,
            "UUID": "661c8dbe-7bc8-4afa-9949-8873f0cdc649",
            "VersionStamp": 1
        },
        {
            "_id": {
                "value": "BASE*BaseUser*0***af70d55a-0a11-4783-bdd4-faae17227c52"
            },
            "AdminUser": false,
            "AnonymousLogin": false,
            "CreationDate": null,
            "CreatorDisplay": null,
            "CreatorName": null,
            "CreatorValue": null,
            "DailyHours": 0.0,
            "Display": "testuser",
            "Display_DISPLAY": "testuser",
            "DisplayedName": "testuser",
            "Division": null,
            "DomainAccount": null,
            "Email": "a@b.de",
            "Enabled": false,
            "Extern": null,
            "FirstName": null,
            "FullName": "testuser",
            "HoursEachDay": null,
            "HoursFriday": null,
            "HoursMonday": null,
            "HoursThursday": null,
            "HoursTuesday": null,
            "HoursWednesday": null,
            "Id": 2,
            "Internal": false,
            "InvalidLoginAttempts": null,
            "LastActivity": null,
            "LastModified": null,
            "LastName": null,
            "LastNameOrUserName": "testuser",
            "LastPasswordChange": null,
            "Mobile": null,
            "Phone": null,
            "ResetMonthlyHours": null,
            "ResourcePlanAccess": null,
            "SerialNumber": null,
            "TimeSheetAccess": null,
            "TimesheetRequired": null,
            "Title": null,
            "UserShortName": null,
            "UUID": "af70d55a-0a11-4783-bdd4-faae17227c52",
            "VersionStamp": 0
        }
    ],
    "details": {
        "serverInvocationID": "f8736e86-16"
    }
}
  ''';