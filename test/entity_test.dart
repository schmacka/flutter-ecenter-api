import 'dart:convert';

import 'package:ecenter_api/ecenter_api.dart';
import 'package:flutter_test/flutter_test.dart';

import 'testdata/base_user.dart';
import 'testdata/test_class.dart';

void main() {
  test('parse_entity_contains_id', () {
    var json = jsonDecode(baseUserData);
    expect(Entity.fromJson(json).id.id, equals("661c8dbe-7bc8-4afa-9949-8873f0cdc649"));
  });

  test('parse_subclass_contains_id', () {
    var json = jsonDecode(baseUserData);
    expect(BaseUser.fromJson(json).id.id, equals("661c8dbe-7bc8-4afa-9949-8873f0cdc649"));
  });

  test('parse_entity_based_on_type_is_user', () {
    var json = jsonDecode(baseUserData);
    EntityTypeRegistry api = EntityTypeRegistry();
    api.register(BaseUser, (json) => BaseUser.fromJson(json));
    Entity e = api.fromJson(json, api);
    expect(e, isInstanceOf<BaseUser>());
    expect(e.id.type, equals("BaseUser"));
  });

  test('emptyEntity_hasInitializedIdAndJson', () {
    Entity e = Entity();
    expect(e.id, equals(ClientIDAttribute.nullId));
    expect(e.json, {});

  });

  test('parse_entity_based_on_type_is_test_class', () {
    var json = jsonDecode(testClass);
    EntityTypeRegistry api = EntityTypeRegistry();
    api.register(TestClass, (json) => TestClass.fromJson(json));
    Entity e = api.fromJson(json, api);
    expect(e, isInstanceOf<TestClass>());
    expect(e.id.type, equals("TestClass"));
  });

  test('parse_unknown_entity_based_is_entity_class', () {
    var json = jsonDecode(testClass);
    EntityTypeRegistry api = EntityTypeRegistry();
    Entity e = api.fromJson(json, api);
    expect(e, isInstanceOf<Entity>());
  });
}

var testClass = '''
{
            "_id": {
                "value": "BASE*TestClass*1***661c8dbe-7bc8-4afa-9949-8873f0cdc649"
            },
            "data": true
}
''';

var baseUserData = '''
{
            "_id": {
                "value": "BASE*BaseUser*1***661c8dbe-7bc8-4afa-9949-8873f0cdc649"
            },
            "AdminUser": true,
            "AnonymousLogin": false,
            "CreationDate": "2007-11-05T23:00:00Z",
            "CreatorDisplay": "",
            "CreatorName": "",
            "CreatorValue": null,
            "DailyHours": 8.0,
            "Display": "admin",
            "Display_DISPLAY": "admin",
            "DisplayedName": "admin",
            "Division": null,
            "DomainAccount": null,
            "Email": null,
            "Enabled": true,
            "Extern": false,
            "FirstName": null,
            "FullName": "admin",
            "HoursEachDay": 8.0,
            "HoursFriday": 0.0,
            "HoursMonday": 0.0,
            "HoursThursday": 0.0,
            "HoursTuesday": 0.0,
            "HoursWednesday": 0.0,
            "Id": 1,
            "Internal": false,
            "InvalidLoginAttempts": 0,
            "LastActivity": "2021-05-28T15:35:02.225+02:00",
            "LastModified": "2021-05-28T09:37:31.803Z",
            "LastName": null,
            "LastNameOrUserName": "admin",
            "LastPasswordChange": "2021-05-28T11:37:31.734+02:00",
            "Mobile": null,
            "Phone": null,
            "ResetMonthlyHours": false,
            "ResourcePlanAccess": false,
            "SerialNumber": null,
            "TimeSheetAccess": false,
            "TimesheetRequired": false,
            "Title": null,
            "UserName": "admin",
            "UserShortName": null,
            "UUID": "661c8dbe-7bc8-4afa-9949-8873f0cdc649",
            "VersionStamp": 1
        }
''';
