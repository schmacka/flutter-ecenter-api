import 'dart:io';

import 'package:ecenter_api/ecenter_api.dart';
import 'package:flutter_test/flutter_test.dart';
import 'dart:convert';

import 'testdata/base_user.dart';

void main() {

  test('id_regex_matches_correct_id', () {
    expect(ClientIDAttribute.idMatcher.hasMatch("SYSTEM*TYPE*VERSION***ID"), isTrue);
  });

  test('id_from_system_and_type', () {
    expect(ClientIDAttribute(system: "SYS", type: "TYPE", version: "0", id: "id").value, equals("SYS*TYPE*0***id"));
  });

  test('id_regex_fails_incorrect_id1', () {
    expect(ClientIDAttribute.idMatcher.hasMatch("TYPE*VERSION***ID"), isFalse);
  });

  test('id_regex_fails_incorrect_id2', () {
    expect(ClientIDAttribute.idMatcher.hasMatch("SYS*TYPE*VERSION*ID"), isFalse);
  });

  test('deserialize_listOfBean_hasClientIdAttribute', () {
    var json = jsonDecode(queryResult);

    List<BaseUser>result = [];
    json["result"].forEach((v) {
      result.add(BaseUser.fromJson(v));
    });
    expect(result, hasLength(2));
    expect(result[0].id.id, equals("661c8dbe-7bc8-4afa-9949-8873f0cdc649"));
  });

  test('deserialize_ClientIDAttribute', () {
    var json = jsonDecode(id);

    var parsed = ClientIDAttribute.fromJson(json);
    expect(parsed.value, equals("BASE*BaseUser*1***661c8dbe-7bc8-4afa-9949-8873f0cdc649"));
  });

  test('deserialize_ClientIDAttribute_fromString', () {
    var json = jsonDecode('"BASE*BaseUser*1***661c8dbe-7bc8-4afa-9949-8873f0cdc649"');

    var parsed = ClientIDAttribute.fromJson(json);
    expect(parsed.value, equals("BASE*BaseUser*1***661c8dbe-7bc8-4afa-9949-8873f0cdc649"));
  });

  test('missing_value_throwsException', () {
    var json = jsonDecode(defectId);
    expect(() =>ClientIDAttribute.fromJson(json),throwsException);

  });

  test('invalid_value_throwsException', () {
    var json = jsonDecode('"***"');
    expect(() =>ClientIDAttribute.fromJson(json),throwsException);

  });

  test('clientid_hasType_fromString', () {
    ClientIDAttribute parsed = buildBBBAAA1DDD();
    expect(parsed.type, equals("AAA"));
  });

  test('clientid_hasSystem_fromString', () {
    ClientIDAttribute parsed = buildBBBAAA1DDD();
    expect(parsed.system, equals("BBB"));
  });

  test('clientid_hasId_fromString', () {
    ClientIDAttribute parsed = buildBBBAAA1DDD();
    expect(parsed.id, equals("DDD"));
  });

  test('clientid_hasId_fromString', () {
    var json = jsonDecode('"BBB*AAA*3***DDD"');
    var parsed = ClientIDAttribute.fromJson(json);
    expect(parsed.version, equals("3"));
  });


  test('clientid_emptyProperties', () {
    ClientIDAttribute parsed =  ClientIDAttribute();
    expect(parsed.id, equals(""));
    expect(parsed.system, equals(ClientIDAttribute.nullSystem));
    expect(parsed.type, equals(ClientIDAttribute.nullType));
    expect(parsed.version, equals(ClientIDAttribute.nullVersion));
  });
}

ClientIDAttribute buildBBBAAA1DDD() {
  var json = jsonDecode('"BBB*AAA*1***DDD"');
  var parsed = ClientIDAttribute.fromJson(json);
  return parsed;
}


String id = '''
{
    "value": "BASE*BaseUser*1***661c8dbe-7bc8-4afa-9949-8873f0cdc649"
}
''';

String defectId = '''
{
    "defect": ""
}
''';

String queryResult = '''
  {
    "result": [
        {
            "_id": {
                "value": "BASE*BaseUser*1***661c8dbe-7bc8-4afa-9949-8873f0cdc649"
            },
            "AdminUser": true,
            "AnonymousLogin": false,
            "CreationDate": "2007-11-05T23:00:00Z",
            "CreatorDisplay": "",
            "CreatorName": "",
            "CreatorValue": null,
            "DailyHours": 8.0,
            "Display": "admin",
            "Display_DISPLAY": "admin",
            "DisplayedName": "admin",
            "Division": null,
            "DomainAccount": null,
            "Email": null,
            "Enabled": true,
            "Extern": false,
            "FirstName": null,
            "FullName": "admin",
            "HoursEachDay": 8.0,
            "HoursFriday": 0.0,
            "HoursMonday": 0.0,
            "HoursThursday": 0.0,
            "HoursTuesday": 0.0,
            "HoursWednesday": 0.0,
            "Id": 1,
            "Internal": false,
            "InvalidLoginAttempts": 0,
            "LastActivity": "2021-05-28T15:35:02.225+02:00",
            "LastModified": "2021-05-28T09:37:31.803Z",
            "LastName": null,
            "LastNameOrUserName": "admin",
            "LastPasswordChange": "2021-05-28T11:37:31.734+02:00",
            "Mobile": null,
            "Phone": null,
            "ResetMonthlyHours": false,
            "ResourcePlanAccess": false,
            "SerialNumber": null,
            "TimeSheetAccess": false,
            "TimesheetRequired": false,
            "Title": null,
            "UserName": "admin",
            "UserShortName": null,
            "UUID": "661c8dbe-7bc8-4afa-9949-8873f0cdc649",
            "VersionStamp": 1
        },
        {
            "_id": {
                "value": "BASE*BaseUser*0***af70d55a-0a11-4783-bdd4-faae17227c52"
            },
            "AdminUser": false,
            "AnonymousLogin": false,
            "CreationDate": null,
            "CreatorDisplay": "",
            "CreatorName": "",
            "CreatorValue": null,
            "DailyHours": 0.0,
            "Display": "testuser",
            "Display_DISPLAY": "testuser",
            "DisplayedName": "testuser",
            "Division": null,
            "DomainAccount": null,
            "Email": "a@b.de",
            "Enabled": true,
            "Extern": null,
            "FirstName": null,
            "FullName": "testuser",
            "HoursEachDay": null,
            "HoursFriday": null,
            "HoursMonday": null,
            "HoursThursday": null,
            "HoursTuesday": null,
            "HoursWednesday": null,
            "Id": 2,
            "Internal": false,
            "InvalidLoginAttempts": null,
            "LastActivity": null,
            "LastModified": null,
            "LastName": null,
            "LastNameOrUserName": "testuser",
            "LastPasswordChange": null,
            "Mobile": null,
            "Phone": null,
            "ResetMonthlyHours": null,
            "ResourcePlanAccess": null,
            "SerialNumber": null,
            "TimeSheetAccess": null,
            "TimesheetRequired": null,
            "Title": null,
            "UserName": "testuser",
            "UserShortName": null,
            "UUID": "af70d55a-0a11-4783-bdd4-faae17227c52",
            "VersionStamp": 0
        }
    ],
    "details": {
        "serverInvocationID": "f8736e86-16"
    }
}
  ''';
