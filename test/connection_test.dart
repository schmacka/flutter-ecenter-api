import 'package:ecenter_api/ecenter_api.dart';
import 'package:ecenter_api/src/connection.dart';
import 'package:flutter_test/flutter_test.dart';

import 'testdata/fail_login_plugin_mock.dart';
import 'testdata/login_plugin_mock.dart';

void main() {

  test('get_token_Exception_when_plugin_missing', () {
    Connection connection = Connection("http://test", "testSystem");
    expect(connection.getToken(), isNull);
  });

  test('is_logged_in_Exception_when_plugin_missing', () {
    Connection connection = Connection("http://test", "testSystem");
    expect(connection.isLoggedIn(), isFalse);
  });

  test('login_Exception_when_plugin_missing', () async {
    Connection connection = Connection("http://test", "testSystem");
    expect(() => connection.login(), throwsException);
  });

  test('loginFails_resets_state', () async {
    Connection connection = Connection("http://test", "testSystem");
    ThrowningLoginPluginMock loginPlugin = ThrowningLoginPluginMock();
    connection.loginPlugin = loginPlugin;
    try {
      final loginResult = await connection.login();
    } catch (e) {
      print(e);
    }
    expect(connection.isLoginInProgress(), isFalse);
    expect(connection.isLoggedIn(), isFalse);
  });

  test('login', () async {
    Connection connection = Connection("http://test", "testSystem");
    LoginPluginMock loginPlugin = LoginPluginMock();
    connection.loginPlugin = loginPlugin;
    final loginResult = await connection.login();
    expect(loginResult is LoginResult, isTrue);
    expect(loginResult.loginSuccessful, isTrue);
  });

  test('is_logged_in', () async {
    Connection connection = Connection("http://test", "testSystem");
    LoginPluginMock loginPlugin = LoginPluginMock();
    connection.loginPlugin = loginPlugin;
    expect(connection.isLoggedIn(), isFalse);
  });

  test('login_with_mock_plugin', () async {
    Connection connection = Connection("http://test", "testSystem");
    LoginPluginMock loginPlugin = MockLoginPlugin(connection);
    connection.loginPlugin = loginPlugin;

    bool initResult = await connection.loginInitialized();
    expect(initResult, isFalse);
    expect(connection.isLoggedIn(), isFalse);

    final loginResult = await connection.login();

    expect(loginResult.loginSuccessful, isTrue);
    expect(loginResult.userName, 'testUser');
    expect(loginResult.token, '123456');
  });


  test('loginInitialized_shouldNotReplaceTokenInInitializeLogin', () async {
    Connection connection = Connection("http://test", "testSystem", token: "someToken");
    LoginPluginMock loginPlugin = MockLoginPlugin(connection);
    connection.loginPlugin = loginPlugin;

    bool initResult = await connection.loginInitialized();
    expect(connection.getToken(), equals("someToken"));

  });

  test('login_with_mock_oauth_plugin', () async {
    Connection connection = Connection("http://test", "testSystem");
    LoginPluginMock loginPlugin = MockOAuthLoginPlugin(connection);
    connection.loginPlugin = loginPlugin;

    bool initResult = await connection.loginInitialized();
    expect(initResult, isTrue);

    expect(connection.isLoggedIn(), isTrue);
    expect(connection.getToken(), '123456');

    final loginResult = await connection.login();

    expect(loginResult.loginSuccessful, isTrue);
    expect(loginResult.userName, isNull);
    expect(loginResult.token, isNull);
  });
}

class MockLoginPlugin implements LoginPluginMock {

  Connection _connection;

  MockLoginPlugin(this._connection);

  @override
  Future<LoginResult> login() async {
    String token = '123456';
    return LoginResult(loginSuccessful: true, allRoles: ['admin'], userName: 'testUser', token: token);
  }

  @override
  Future<bool> logout() async {
    return true;
  }

  @override
  Future<String?> loginInitialized() async{
   return null;
  }
}

class MockOAuthLoginPlugin implements LoginPluginMock {

  Connection _connection;

  MockOAuthLoginPlugin(this._connection);

  @override
  Future<LoginResult> login() async {
    return LoginResult(loginSuccessful: true);
  }

  @override
  Future<bool> logout() async {
    return true;
  }

  @override
  Future<String?> loginInitialized() async{
   return '123456';
  }
}