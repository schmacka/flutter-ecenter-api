import 'dart:convert';
import 'dart:io';

import 'package:ecenter_api/ecenter_api.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';

var nullResult = '{"result":null}';
void main() {
  MockClient mockClient() {
    return MockClient((request) async {
      return Response("", 400);
    });
  }

  test('call_callsHttp', () async {
    bool called = false;
    MockClient client =  MockClient((request) async {
      called = true;
      return Response(nullResult, 200);
    });
    final service = ServiceApi("http://test", httpClient: client);
    Future result = service.callService("query", []);
    var s = await result;
    expect(s, isNull);
    expect(called, isTrue);
  });

  test('queryRequest_buildsQueryUrl', () async {
    MockClient client =  MockClient((request) async {
      expect(request.url.toString(), equals("http://test/v2/services/query"));
      return Response(nullResult, 200);
    });
    final service = ServiceApi("http://test", httpClient:  client);
    Future result = service.callService("query", []);
    await result;
  });

  test('queryRequest_buildsQueryUrl', () async {
    MockClient client =  MockClient((request) async {
      expect(request.url.toString(), equals("http://test/v2/services/query"));
      return Response(nullResult, 200);
    });
    final service = ServiceApi("http://test",httpClient:  client);
    Future result = service.callService("query", []);
    await result;
  });

  test('serviceRequest_containsToken', () async {
    MockClient client =  MockClient((request) async {
      expect(request.headers["Authorization"], equals("Bearer XXX"));
      return Response(nullResult, 200);
    });
    final service = ServiceApi("http://test", httpClient: client, token:"XXX");
    await service.callService("query", []);

  });


  test('httpRequestBody_containsPassedParameter', () async {
    var data =  {"parameter" : ["first","second"]};
    MockClient client =  MockClient((request) async {
      var body = jsonDecode(request.body);
      expect(body, equals(data));
      return Response(nullResult, 200);
    });
    final service = ServiceApi("http://test", httpClient: client);
    await service.callService("query", ["first","second"]);
  });

 test('serviceCallResponse_requiresProperlyFormattedResponse', () async {
    MockClient client =  MockClient((request) async {
      return Response('"hello"', 200);
    });
    final service = ServiceApi("http://test", httpClient: client);

    expect(() async {
      var s  =await service.callService("xxx", []);
    }, throwsException);
  });

 test('serviceCallResponse_containsGivenJsonMap', () async {
    MockClient client =  MockClient((request) async {
      return Response('{"result":{"data":1}}', 200);
    });
    final service = ServiceApi("http://test", httpClient: client);
    var s  =await service.callService("xxx", []);
    expect(s["data"], equals(1));
  });

 test('serviceCall_containsExpandAttributes', () async {
    MockClient client =  MockClient((request) async {
      var body = jsonDecode(request.body);
      expect(body["expand"], equals(["a","b"]));
      return Response(nullResult, 200);
    });
    final service = ServiceApi("http://test", httpClient: client);
    var s  =await service.callService("xxx", [], expandattributes: ["a","b"]);
  });



}
